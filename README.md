### Figuras ###

Este es un ejercicio que busca hacer uso del paradigma orientado objetos. Buscando cumplir todos los principiso del dise�o.

### Explicaci�n de Funcionamiento ###

El modelo se contruyo en base a una clase denominada Cliente que determinaba dos puntos, dados esos dos puntos son enviados a la clase abstracta Figura, que mediante una agregacion hacia la clase Punto, quien es la encargada de calcular la distancia entre los dos puntos dados por lo Clientes, dicha distancia es retornada a la clase Figura. Desde alli, se determina a que figura determinada se envia esta distancia para que mediante su forma especifica de calcular el area y el perimetro determine dichos valores matematicos. 

Cada figura mediante el metodo de calcularArea y calcularPerimetro hace el respectivo calculo y envia mediante consola el resultado que el cliente necesita.

### Modelo ###

* Se encuentra en Diagrama de src/Documentacion/Clases.png

### Integrantes ###

* 20141020053 Johan Sebastian Bonilla Plata
* 20141020096 Javier Duvan Hospital Melo
* 20141020135 Miguel �ngel Hern�ndez Cubillos