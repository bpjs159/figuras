/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras;

/**
 *
 * @author estudiantes
 */
public class Punto {
    
    float x,y;
    
    Punto (float x, float y){
        this.x=x;
        this.y=y;
    }
    
    /* Este método nos permitará calcular la distancia entre los puntos, recibirá dos atributos uno para X y otro para Y */
    
    double calcularDistancia(Punto punto){
        return  Math.sqrt(Math.pow((punto.x-this.x),2)+Math.pow((punto.y-this.y),2));
    }
}
