/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras;

/** Este es una abstracción de la Figura Circulo, que permite calcular el área y el perimetro dos puntos dados
 *
 * @author estudiantes
 */
public class Circulo extends Figura {

    public Circulo(Punto punto1, Punto punto2) {
        super(punto1, punto2);
    }
    

    @Override
    double calcularArea() {
        return Math.PI*Math.pow(punto1.calcularDistancia(punto2),2);
    }

    @Override
    double calcularPerimetro() {
        return 2*Math.PI*punto1.calcularDistancia(punto2);
    }
    
}
