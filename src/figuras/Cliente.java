/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras;

/**
 *
 * @author estudiantes
 */
public class Cliente {
    
      /**
     * Este cliente va a utilizar la clase abstracta figura para poder calcular el área y el perimetro de un circulo. de un cuadrado y de un rectángulo
     */
    public static void main(String[] args) {
        /** Implementación Cuadrado
         * 
         */
        Punto puntoUno= new Punto(0,0);
        Punto puntoDos= new Punto(5,5);
        
        
        Figura cuadrado = new Cuadrado(puntoUno,puntoDos);
        Figura circulo = new Circulo(puntoUno, puntoDos);
        Figura rectangulo = new Rectangulo(puntoUno, puntoDos);
        
        
        System.out.println("El área del cuadrado es: "+cuadrado.calcularArea());
        System.out.println("El perimetro del cuadrado es: "+cuadrado.calcularPerimetro());
        
        System.out.println("El área del circulo es: "+circulo.calcularArea());
        System.out.println("El perimetro del cuadrado es: "+circulo.calcularPerimetro());
        
        System.out.println("El área del rectangulo es: "+rectangulo.calcularArea());
        System.out.println("El perimetro del rectangulo es: "+rectangulo.calcularPerimetro());
    }
}
