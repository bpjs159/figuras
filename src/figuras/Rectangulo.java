/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras;

/** Este es una abstracción de la Figura Rectangulo, que permite calcular el área y el perimetro dos puntos dados. 
 * De forma adicional se trabaja con un punto adicional, para poder proyectarlo y hallar las coordenadas para
 * calcular el area y el perimetro
 *
 * @author estudiantes
 */
public class Rectangulo extends Figura{
    
    private Punto puntoAux;
     public Rectangulo(Punto punto1, Punto punto2) {
        super(punto1, punto2);
        this.puntoAux = new Punto(punto1.x,punto2.y);
    }
    

    @Override
    double calcularArea() {
        return (puntoAux.y-punto1.y)*(punto2.x-puntoAux.x);
    }

    @Override
    double calcularPerimetro() {
        return 2*(puntoAux.y-punto1.y)+2*(punto2.x-puntoAux.x);
    }
}
