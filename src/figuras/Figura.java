/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras;

/**
 *
 * @author estudiantes
 */
public abstract class Figura {
/** Se asocia el punto, para el posterior calculo del área y el cálculo del perimetro 
*El método calcular área no recibe nada y devolverá el área de la figura dada
* El método perimetro no recibe nada y devolverá el perimetro de la figura dada
*/
    
    Punto punto1;
    Punto punto2;
    
    Figura (Punto punto1, Punto punto2){
        this.punto1 = punto1;
        this.punto2 = punto2;
    }
abstract double calcularArea();
abstract double calcularPerimetro();
        
}
